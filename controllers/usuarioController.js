const Usuario = require('../models/Usuario');
const bcrypt = require('bcryptjs')
const {validationResult} = require('express-validator')
const jwt = require('jsonwebtoken')

exports.crearUsuario = async (req, res) => {
  
    // revisar si hay errores
    const errores = validationResult(req)
    if(!errores.isEmpty()){
        return res.status(400).json({errores: errores.array()})
    }



    const {email, password } = req.body

    try {
        //revisar que el usuario registrado sea unico
        
      let usuario = await Usuario.findOne({ email});

      if(usuario){
          return res.status(400).json({
              msg: 'El usuario ya existe'
          })
      }

      // crea el nuevo usuario
      usuario = new Usuario(req.body);

      // Hashear el password
      const salt = await bcrypt.genSalt(10);
      usuario.password = await bcrypt.hash(password, salt)

      // guardar usuario
      await usuario.save();

      // crear y firmar el JWT
      const payload = {
        usuario: {
          id: usuario.id
        }

      }

      // firmar el JWT
      jwt.sign(payload, process.env.SECRETA, {
        expiresIn: '1h'
      }, (error, token)  => {
        if(error) throw error;
        
        // mensaje de confirmacion
        res.json({ token })
      })

     

  } catch (error) {
    console.log(error);
    res.status(400).send('hubo un error')
  }
};
