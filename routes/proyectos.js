const express = require('express');
const router = express.Router();
const proyectoController = require('../controllers/proyectoController')
const auth = require('../middleware/auth')
const { check } = require('express-validator')


// api/proyectos [crear un proyecto]
router.post('/', auth, 
[
    check('nombre', 'El nombre del proyecto es obligatorio').not().isEmpty()
], proyectoController.crearProyecto)


// api/proyectos [obtener todos los proyectos]
router.get('/', auth, proyectoController.obtenerProyectos)


// api/proyectos/:id [actualizar un proyecto]
router.put('/:id', auth,
[
    check('nombre', 'El nombre del proyecto es obligatorio').not().isEmpty()
    
],
proyectoController.actualizarProyecto)


// api/proyectos/:id [eliminar un proyecto]
router.delete('/:id', auth, proyectoController.eliminarProyecto)

module.exports = router